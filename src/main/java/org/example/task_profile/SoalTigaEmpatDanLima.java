package org.example.task_profile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SoalTigaEmpatDanLima {
    public static void main(String[] args) {
        UserProfile uP1 = new UserProfile();
        uP1.setId(1);
        uP1.setName("Rara");
        uP1.setDateOfBirth("May");
        uP1.setSalary(13000);
        UserProfile uP2 = new UserProfile();
        uP2.setId(2);
        uP2.setName("Ellen");
        uP2.setDateOfBirth("May");
        uP2.setSalary(13000);
        UserProfile uP3 = new UserProfile();
        uP3.setId(3);
        uP3.setName("caca");
        uP3.setDateOfBirth("June");
        uP3.setSalary(20000);
        UserProfile uP4 = new UserProfile();
        uP4.setId(4);
        uP4.setName("Alzena");
        uP4.setDateOfBirth("Feb");
        uP4.setSalary(30000);
        UserProfile uP5 = new UserProfile();
        uP5.setId(5);
        uP5.setName("Zehra");
        uP5.setDateOfBirth("June");
        uP5.setSalary(40000);

        List<String> list = new ArrayList<>(); //Soal no. 5
        list.add(uP1.getDateOfBirth());
        list.add(uP2.getDateOfBirth());
        list.add(uP3.getDateOfBirth());
        list.add(uP4.getDateOfBirth());
        list.add(uP5.getDateOfBirth());




        Map<String, Long> counted = list.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        ArrayList<Integer> listSummary = new ArrayList<>();
        listSummary.add(uP1.getSalary());
        listSummary.add(uP2.getSalary());
        listSummary.add(uP3.getSalary());
        listSummary.add(uP4.getSalary());
        listSummary.add(uP5.getSalary());

        int sum = 0;
        for (int num : listSummary) {
            sum += num;
        }
        //double average = (double)sum / listSummary.size();


        System.out.println("Soal nomer 3. buat list nama dari userProfiles");
        System.out.println(uP1.getId()+". Name : "+uP1.getName()+" | Salary : "+uP1.getSalary()+" | DateOfBirth : "+uP1.getDateOfBirth());
        System.out.println(uP2.getId()+". Name : "+uP2.getName()+" | Salary : "+uP2.getSalary()+" | DateOfBirth : "+uP2.getDateOfBirth());
        System.out.println(uP3.getId()+". Name : "+uP3.getName()+" | Salary : "+uP3.getSalary()+" | DateOfBirth : "+uP3.getDateOfBirth());
        System.out.println(uP4.getId()+". Name : "+uP4.getName()+" | Salary : "+uP4.getSalary()+" | DateOfBirth : "+uP4.getDateOfBirth());
        System.out.println(uP5.getId()+". Name : "+uP5.getName()+" | Salary : "+uP5.getSalary()+" | DateOfBirth : "+uP5.getDateOfBirth());
        System.out.println("------------------------------------------------------------------------------------------------------------");
        System.out.println("Soal nomer 4. Sum total salary yang ada di dalam userProfiles");
        System.out.println("Sum: " + sum);
        //System.out.println("Average: " + average);
        System.out.println("------------------------------------------------------------------------------------------------------------");
        System.out.println("Soal nomer 5. Grouping by Month ");
        for (String val : list){
            System.out.println(val);
        }
        System.out.println(counted);
    }
}
